const Koa = require('koa')
const Logger = require('koa-logger')
const router = require('./routes')
const bodyParser = require('koa-bodyparser')
const mongoose = require('mongoose')
const graphqlServer = require('./graphql/graphqlServer')
const { kafkaServer } = require('./messaging/kafka')
const serve = require('koa-static')
const mount = require('koa-mount')
const koaSwagger = require('koa2-swagger-ui')

const PORT = process.env.PORT || 3000

const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('we are connected!')
})
mongoose.connect(`mongodb://localhost:27017/micro-test`, {
  useNewUrlParser: true,
})

const app = new Koa()
app.use(bodyParser())
app.use(Logger())
app.use(mount('/static', serve('./static')))
app.use(router())
app.use(graphqlServer.getMiddleware())
app.use(
  koaSwagger({
    swaggerOptions: {
      url: 'static/swagger.json',
    },
  })
)
kafkaServer.run().catch(console.error)

app.listen(PORT, () => {
  console.log('Server running on port ' + PORT)
  console.log(
    `🚀 GraphQL Server ready at http://localhost:3000${graphqlServer.graphqlPath}`
  )
})
