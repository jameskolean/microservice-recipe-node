const combineRouters = require('koa-combine-routers')
const rootRouter = require('./root')
const todoRouter = require('./todo')
const messageRouter = require('./message')

const router = combineRouters(rootRouter, todoRouter, messageRouter)

module.exports = router
