const Router = require('koa-router')
const router = new Router({ prefix: '/todo' })
const controller = require('../controllers/todo')

router.get('/', controller.findAll)
router.post('/', controller.create)
router.post('/:id', controller.update)
router.put('/:id', controller.update)
router.delete('/:id', controller.destroy)

module.exports = router
