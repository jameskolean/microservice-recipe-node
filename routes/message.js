const Router = require('koa-router')
const router = new Router({ prefix: '/message' })
const controller = require('../controllers/todo')

router.post('/todo', controller.sendMessage)

module.exports = router
