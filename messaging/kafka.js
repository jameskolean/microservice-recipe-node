const { Kafka } = require('kafkajs')
const model = require('../models/todo')

const kafka = new Kafka({
  clientId: 'my-app',
  brokers: ['localhost:9092'],
})

const producer = kafka.producer()
const consumer = kafka.consumer({ groupId: 'group_id' })
const kafkaServer = {
  run: async () => {
    // Consuming
    await consumer.connect()
    await consumer.subscribe({ topic: 'todo', fromBeginning: true })

    await consumer.run({
      eachMessage: async ({ topic, partition, message }) => {
        console.log({
          partition,
          offset: message.offset,
          value: message.value.toString(),
        })
        try {
          const payload = JSON.parse(message.value.toString())
          const newTodo = new model({
            description: payload.description,
            completed: false,
          })
          const savedTodo = await newTodo.save()
        } catch (error) {
          console.error('Unable to process messasge', message.value.toString())
        }
      },
    })
  },
}

module.exports = { kafkaServer, producer }
