const mongoose = require('mongoose')

// Declare Schema
const TodoSchema = new mongoose.Schema(
  {
    description: { type: String },
    completed: { type: Boolean, default: false },
  },
  {
    timestamps: true,
    // , _id: false
  }
)

// Declare Model to mongoose with Schema
const Todo = mongoose.model('Todo', TodoSchema)

// Export Model to be used in Node
module.exports = mongoose.model('Todo')
