const { gql } = require('apollo-server-koa')
const model = require('../../models/todo')

const typeDef = gql`
  type Todo {
    id: String
    version: Int
    description: String!
    completed: Boolean!
  }
  extend type Query {
    Todos: [Todo]
  }
  extend type Mutation {
    createTodo(description: String): Todo
    completeTodo(id: String): Todo
    deleteTodo(id: String): Todo
  }
`
const resolvers = {
  Todo: {
    id: (val) => val._id,
    version: (val) => val.__v,
  },
  Query: {
    Todos: async () => {
      return await model.find({}).lean()
    },
  },
  Mutation: {
    createTodo: async (root, { description }) => {
      const newTodo = new model({ description, completed: false })
      return await newTodo.save()
    },
    completeTodo: async (root, { id }) => {
      const todo = await model.findById(id)
      todo.completed = !todo.completed
      return await todo.save()
    },
    deleteTodo: async (root, { id }) => {
      const todo = await model.findById(id)
      if (todo) return await todo.remove()
      return null
    },
  },
}
module.exports = {
  typeDef,
  resolvers,
}
