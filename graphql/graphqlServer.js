const { ApolloServer, gql } = require('apollo-server-koa')
const { makeExecutableSchema } = require('graphql-tools')
const merge = require('lodash/merge')
const { typeDef: Todo, resolvers: TodoResolvers } = require('./types/todo')
const Query = gql`
  type Query {
    hello: String
  }

  type Mutation {
    # dummy entry
    null: Boolean
  }
`

const SchemaDefinition = gql`
  schema {
    query: Query
    mutation: Mutation
  }
`

const resolvers = {
  Query: {
    hello: () => 'Hello world!',
  },
}

const schema = makeExecutableSchema({
  typeDefs: [SchemaDefinition, Query, Todo],
  resolvers: merge(resolvers, TodoResolvers),
})

module.exports = new ApolloServer({ schema })
