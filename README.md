# Start Kafka

start docker container

```
docker-compose -f ./kafka/docker-compose.yml up -d
```

### Stop

```
docker-compose -f ./kafka/docker-compose.yml stop
```

### Monitor topic

```
kafka-console-consumer --bootstrap-server localhost:9092 --topic todo
```

### Send test Messages

kafka-console-producer --broker-list localhost:9092 --topic todo

# Run Node Server

npm run dev

# Generate Swagger spec

- Go to https://inspector.swagger.io/builder
- Make sone requests
- Check some in History that click CREAET API DEFINITION
- Export from SwaggerHub https://app.swaggerhub.com
- Copy the contents into /swagger-spec.json
