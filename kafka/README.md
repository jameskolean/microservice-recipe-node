# Start Kafka

```console
docker-compose up -d
docker ps
```

# Kafka on the commandline

These command will:

1. Start an interactive producer. Once running you can type a message like: Hello World<Return> to add a message to Kafka. Note: you will need to install Kafka to have access to kafka-console-producer, I suggest using Homebrew to install this `brew install kafka`.

```console
kafka-console-producer --broker-list localhost:9092 --topic test
```

2. Start a consumer to monitor the queue.

```console
kafka-console-consumer --bootstrap-server localhost:9092 --topic test
```
